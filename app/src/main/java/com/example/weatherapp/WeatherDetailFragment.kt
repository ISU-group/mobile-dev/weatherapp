package com.example.weatherapp

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LifecycleOwner
import coil.load
import com.example.weatherapp.databinding.FragmentWeatherDetailBinding
import com.example.weatherapp.databinding.FragmentWeatherShortBinding


class WeatherDetailFragment : Fragment() {
    private lateinit var binding: FragmentWeatherDetailBinding
    private val weatherModel: WeatherModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentWeatherDetailBinding.inflate(inflater)
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        weatherModel.data.observe(activity as LifecycleOwner) {
            updateData()
        }
    }

    fun updateData() {
        weatherModel.data.value.also {
            if (it != null) {
                binding.apply {
                    tvHumidity.text = context?.getString(R.string.percent, it.main.humidity)
                    tvFeelsLikeTemp.text = context?.getString(R.string.feel_like_temperature, it.main.feelsLike)
                    tvWindSpeed.text = context?.getString(R.string.wind_speed, it.wind.speed)
                    tvWindDir.text = context?.getString(R.string.wind_direction, it.wind.dir)
                }
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = WeatherDetailFragment()
    }
}