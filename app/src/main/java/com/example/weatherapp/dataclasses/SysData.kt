package com.example.weatherapp.dataclasses

data class SysData(
    val sunrise: Long,
    val sunset: Long
)
