package com.example.weatherapp.dataclasses

data class CoordData(
    val lon: Float,
    val lat: Float
)
