package com.example.weatherapp.dataclasses

data class WeatherData(
    val id: UInt,
    val main: String,
    val description: String,
    val icon: String
    )
