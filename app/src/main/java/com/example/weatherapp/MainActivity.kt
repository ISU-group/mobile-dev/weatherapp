package com.example.weatherapp

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.fragment.app.commit
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherapp.databinding.ActivityMainBinding
import com.example.weatherapp.dataclasses.Weather
import com.example.weatherapp.dataclasses.WeatherDeserializer
import com.example.weatherapp.dialogs.LanguageDialog
import com.example.weatherapp.dialogs.SimpleDialog
import com.example.weatherapp.snaphelpers.*
import com.google.android.material.snackbar.Snackbar
import com.google.gson.GsonBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.InputStream
import java.net.URL
import java.util.*

class MainActivity : AppCompatActivity(), OnSnapPositionChangeListener {
    private lateinit var preferences: SharedPreferences
    private lateinit var selectedCity: String
    private lateinit var binding: ActivityMainBinding
    private val weatherModel: WeatherModel by viewModels()
    private var detailed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
        setLocale()
    }

    private fun init() {
        preferences = getSharedPreferences("app_preferences", Context.MODE_PRIVATE)

        supportFragmentManager.commit {
            replace(R.id.fragment_container, WeatherShortFragment.newInstance(), "weatherFragment")
            hide(binding.fragmentWeatherDetail.getFragment())
        }

        binding.apply {
            fragmentWeatherShort.setOnClickListener {
                detailed = !detailed

                if (detailed) {
                    supportFragmentManager.commit { show(fragmentWeatherDetail.getFragment()) }
                } else {
                    supportFragmentManager.commit { hide(fragmentWeatherDetail.getFragment()) }
                }
            }
        }

        initSnapHelper();
//        SimpleDialog(this).show(supportFragmentManager, "SimpleDialog")
    }

    private fun loadWeather() {
        val weatherURL = resources.getString(R.string.open_weather_map_url, selectedCity, BuildConfig.API_KEY)
        var weather: Weather? = null

        try {
            val stream = URL(weatherURL).content as InputStream

            // JSON отдаётся одной строкой,
            val data = Scanner(stream).nextLine()

            val gson = GsonBuilder().registerTypeAdapter(Weather::class.java, WeatherDeserializer()).create()
            weather = gson.fromJson(data, Weather::class.java)
        } catch (e: Exception) {
            Log.e("WeatherException", e.toString())
            popUpMessage("No Internet access")
        }

        if (weather?.cod == 200) {
            weatherModel.data.postValue(weather)
        } else {
            Log.d("WeatherApp", "No Internet access")
        }

        Log.d("WeatherReceivedData", weather.toString())
    }

    private fun initSnapHelper() {
        val pagerSnapHelper = PagerSnapHelper()
        pagerSnapHelper.attachToRecyclerView(binding.rvCities)

        binding.rvCities.apply {
            layoutManager = LinearLayoutManager(this@MainActivity, RecyclerView.HORIZONTAL, false)

            adapter = CityAdapter(layoutInflater).apply {
                val list = 0 until resources.getStringArray(R.array.cities).size
                submitList(list.toList())
            }

            attachSnapHelperWithListener(
                pagerSnapHelper,
                behavior = SnapOnScrollListener.Behavior.NOTIFY_ON_SCROLL,
                onSnapPositionChangeListener = this@MainActivity
            )
        }
    }

    private fun popUpMessage(text: String, duration: Int = Snackbar.LENGTH_LONG) = Snackbar
        .make(binding.root, text, duration).show()

    override fun onSnapPositionChange(position: Int) {
        selectedCity = resources.getStringArray(R.array.cities)[position]

        GlobalScope.launch (Dispatchers.IO) {
            loadWeather()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.overflow_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_language -> {
                LanguageDialog(this) { setLocale() }
                    .show(supportFragmentManager, "LanguageDialog")
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setLocale() {
        val language = preferences.getString("locale", resources.configuration.locale.language)!!

        val configuration = resources.configuration
        configuration.locale = Locale(language)
        resources.updateConfiguration(configuration, resources.displayMetrics)
        onConfigurationChanged(configuration)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)

        updateFragments()
    }

    private fun updateFragments() {
        binding.apply {
            fragmentWeatherShort.getFragment<WeatherShortFragment>().updateData()
            fragmentWeatherDetail.getFragment<WeatherDetailFragment>().updateData()
        }

        Log.d("WeatherAppUpdateFgs", "Updating...")
    }

}

