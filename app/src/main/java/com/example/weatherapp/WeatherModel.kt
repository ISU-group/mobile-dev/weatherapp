package com.example.weatherapp

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.weatherapp.dataclasses.Weather

open class WeatherModel : ViewModel() {
    val data : MutableLiveData<Weather> by lazy {
        MutableLiveData()
    }
}