package com.example.weatherapp

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LifecycleOwner
import coil.load
import com.example.weatherapp.databinding.FragmentWeatherShortBinding


class WeatherShortFragment : Fragment() {
    private lateinit var binding: FragmentWeatherShortBinding
    private val weatherModel: WeatherModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentWeatherShortBinding.inflate(inflater)
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        weatherModel.data.observe(activity as LifecycleOwner) {
            binding.apply {
                updateData()

                val weatherIconURL = context.getString(R.string.weather_icon_url, it.weather.icon)
                ivWeatherIcon.load(weatherIconURL)
            }
        }
    }

    fun updateData() {
        weatherModel.data.value.also {
            if (it != null) {
                binding.apply {
                    tvCurrentTemp.text = context?.getString(R.string.temperature, it.main.temp)
                    tvMinMaxTemp.text = context?.getString(
                        R.string.min_max_temperature,
                        it.main.tempMin,
                        it.main.tempMax
                    )
                    tvDescription.text = it.weather.main
                }
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = WeatherShortFragment()
    }
}