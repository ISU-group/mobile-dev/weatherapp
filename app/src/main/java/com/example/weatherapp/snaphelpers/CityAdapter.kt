package com.example.weatherapp.snaphelpers

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.example.weatherapp.R

class CityAdapter(private val inflater: LayoutInflater)
    : ListAdapter<Int, CityViewHolder>(CityDiffer) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder {
        return CityViewHolder(inflater.inflate(R.layout.city_layout, parent, false))
    }

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    private object CityDiffer : DiffUtil.ItemCallback<Int>() {
        override fun areItemsTheSame(oldItem: Int, newItem: Int): Boolean = oldItem == newItem

        override fun areContentsTheSame(oldItem: Int, newItem: Int): Boolean = areItemsTheSame(oldItem, newItem)
    }
}
