package com.example.weatherapp.snaphelpers

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherapp.R
import com.example.weatherapp.databinding.CityLayoutBinding

class CityViewHolder(private val item: View): RecyclerView.ViewHolder(item) {
    private val binding = CityLayoutBinding.bind(item)

    fun bind(cityIdx: Int) = with(binding) {
        tvCity.text = root.resources.getStringArray(R.array.cities)[cityIdx].toString()
    }
}
