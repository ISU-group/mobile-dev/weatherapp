package com.example.weatherapp.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.example.weatherapp.R
import java.lang.IllegalStateException

class SimpleDialog(val ctx: Context) : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return ctx.let {
            val builder = AlertDialog.Builder(it)
            builder
                .setMessage(R.string.simple_dialog_msg)
                .setPositiveButton(R.string.dialog_ok,
                    DialogInterface.OnClickListener { dialog, id ->
                        Toast
                            .makeText(it, "You just clicked `OK`", Toast.LENGTH_SHORT)
                            .show()
                    })
                .setNeutralButton(R.string.dialog_cancel,
                    DialogInterface.OnClickListener { dialog, i ->
                        Toast
                            .makeText(it, "Ok...", Toast.LENGTH_SHORT)
                            .show()
                    })
                .create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}