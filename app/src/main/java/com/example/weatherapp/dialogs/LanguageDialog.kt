package com.example.weatherapp.dialogs

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.example.weatherapp.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class LanguageDialog(private val ctx: Context, val callback: () -> Unit) : DialogFragment() {
    private val languages = lazy { resources.getStringArray(R.array.languages) }
    private val locales = lazy { resources.getStringArray(R.array.locales) }

    private val preferences = ctx.getSharedPreferences("app_preferences", Context.MODE_PRIVATE)

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        var locale = preferences.getString("locale", resources.configuration.locale.language)!!
        Log.d("WeatherAppLocale", "Locale: $locale")
        val startIdx = locales.value.indexOf(locale)
        Log.d("WeatherAppLocale", "Index: ${startIdx.toString()}")

        return ctx.let {
            MaterialAlertDialogBuilder(it)
                .setTitle(resources.getString(R.string.language))
                .setSingleChoiceItems(languages.value, startIdx) {dialog, which ->
                    locale = locales.value[which]
                }
                .setPositiveButton(R.string.dialog_ok,
                    DialogInterface.OnClickListener { dialog, id ->
                        preferences
                            .edit()
                            .putString("locale", locale)
                            .apply()

                        Log.d("WeatherAppLocaleDialog", locale)

                        Toast
                            .makeText(it, "Locale has been changed: $locale", Toast.LENGTH_SHORT)
                            .show()

                        callback()
                    })
                .create()
        }
    }
}